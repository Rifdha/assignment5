#include<stdio.h>

int attendee(int);
int income (int);
int expenditure(int);
int final_profit(int);
void display();

int attendee(int value){
	const int attendee=120;
	const int price=15;
	return attendee-((price)/5*20);
}

int income(int value){
	return attendee(value)*value;
}

int expenditure(int value){
	const int cost = 500;
	return cost + attendee(value)*3;
}

int final_profit(int value){
	return income(value)-expenditure(value);
}

void display(){
	int x=10,profit;
	for(x=10;x<=40;x+=5);{
		profit=final_profit(x);
		printf("%d\t%d\n",x,profit);
	}
}

int main(){
	printf("Ticket prices and final profits are:\n");
	printf("price\tprofit\n");
	display();
	printf("According to this,thr maximum profit is Rs.1260 in Rs.25 Ticket.");
	return 0;
}

